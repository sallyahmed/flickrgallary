//
//  SwipeViewController.m
//  Flickr
//
//  Created by Sally Ahmed on 2/6/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "SwipeViewController.h"
#import "ImagePage.h"
#import "FlickrModel.h"
@interface SwipeViewController ()

@end

@implementation SwipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillDisappear:(BOOL)animated
{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
    [self.navigationController.navigationBar setHidden:YES];
    [super viewWillDisappear:animated];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
//    [self.swipeView setCurrentItemIndex:self.beginIndex];
  [self.swipeView scrollToItemAtIndex:self.beginIndex duration:.6];
   
}
-(void)viewDidLayoutSubviews{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - swipe -
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView{
    return  [self.photoData count];
}
- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(ImagePage *)view{
    if (view == nil) {
 
    view= [[ImagePage alloc]init];
     view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    view.tag = index;
    
    if (![[self.photoData objectAtIndex:index] isKindOfClass:[NSString class]]) {
    view.pageImage.image = [UIImage imageWithData:[self.photoData objectAtIndex:index]];
        
    }
    else{
        NSURL *url = [NSURL URLWithString:[[[FlickrModel getInstance]getSmallPhotos]objectAtIndex:index]];

        view.pageImage.image =[UIImage imageNamed:@"noimage"];
        [self downloadImageWithURL:url andIndex :(int)index completionBlock:^(BOOL succeeded, NSData *data) {
            if (succeeded) {
                if (view.tag == index) {
                    
                view.pageImage.image = [UIImage imageWithData:[self.photoData objectAtIndex:index]];
                }
                }
        }];

    }
    return view;
    
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}

- (void)downloadImageWithURL:(NSURL *)url andIndex :(int)index completionBlock:(void (^)(BOOL succeeded, NSData *data))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            [self.photoData replaceObjectAtIndex:index withObject:data];
            
            completionBlock(YES, data);
        } else {
            completionBlock(NO, nil);
        }
    }];
}
@end
