//
//  SwipeViewController.h
//  Flickr
//
//  Created by Sally Ahmed on 2/6/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
@interface SwipeViewController : UIViewController<SwipeViewDataSource , SwipeViewDelegate>

@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
@property (nonatomic , strong)NSMutableArray *photoData;
@property (nonatomic)int beginIndex;
@end
