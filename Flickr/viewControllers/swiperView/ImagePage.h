//
//  ImagePage.h
//  Flickr
//
//  Created by Sally Ahmed on 2/6/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePage : UIView
@property (weak, nonatomic) IBOutlet UIImageView *pageImage;
@property (weak, nonatomic) IBOutlet UIView *view;

@end
