//
//  ImagePage.m
//  Flickr
//
//  Created by Sally Ahmed on 2/6/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "ImagePage.h"

@implementation ImagePage

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)init{
    self = [super init];
    if(self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ImagePage" owner:self options:nil];
        
        for (id view in arrayOfViews) {
            if ([view isKindOfClass:[ImagePage class]]) {
                self = view;
                break;
            }

    }
    }
    return self;

}



@end
