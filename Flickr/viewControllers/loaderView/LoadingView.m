//
//  LoadingView.m
//  rad182
//
//  Created by Royce Albert Dy on 4/27/11.
//  Copyright 2011 rad182. All rights reserved.
//

#import "LoadingView.h"
#import "UILabel+Boldify.h"
#import <CoreText/CoreText.h>
#import "ApplicationConstant.h"
@implementation LoadingView
UIView * containerOfAll;
@synthesize loadingLabel,spinner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor colorWithRed:190/255 green:190/255 blue:190/255 alpha:0.2];
        [self setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
        
        container = [[UIView alloc] init];
         containerOfAll = [[UIView alloc] init];
        [containerOfAll setBackgroundColor:UIColorFromRGB(0x000000)];
        containerOfAll.layer.cornerRadius = 10;
        containerOfAll.layer.masksToBounds = YES;
        [container setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin];
   
        
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        [spinner startAnimating];
        [container addSubview:spinner];
        loadingLabel = [[UILabel alloc] init];
        [loadingLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [loadingLabel setBackgroundColor:[UIColor clearColor]];
        [loadingLabel setTextColor:[UIColor whiteColor]];
        [loadingLabel setShadowColor:[UIColor whiteColor]];
        [loadingLabel setShadowOffset:CGSizeMake(0, 1)];
        [loadingLabel setFrame:CGRectMake(0.0f, 0.0f, 500.0f, 20.0f)];
        [loadingLabel setTextAlignment:NSTextAlignmentCenter];
       // [self setAlpha:0.6];
        [self addSubview:containerOfAll];

        [self addSubview:loadingLabel];
        [self addSubview:container];
    // NSLog(@"%f ===" , [loadingLabel contentSize].width);
    }
    return self;
}

- (void)layoutSubviews {
    CGSize viewsize = self.frame.size;
    CGSize spinnersize = [spinner bounds].size;
  //  CGSize textsize;
   // textsize = [[loadingLabel text]  sizeWithFont:[loadingLabel font]];

    float bothwidth = spinnersize.width + 5.0f;//+ textsize.width
    
    CGRect containrect = {
        CGPointMake(floorf((viewsize.width / 2) - (bothwidth / 2)), floorf((viewsize.height / 2) - (spinnersize.height / 2))-20),
        CGSizeMake(bothwidth, spinnersize.height)
    };
//    CGRect textrect = {
//        CGPointMake(spinnersize.width + 5.0f, floorf((spinnersize.height / 2) - (textsize.height / 2))),
//        textsize
//    };
    CGRect spinrect = {
        CGPointZero,
        spinnersize
    };

    [loadingLabel setFrame:CGRectMake((self.frame.size.width - loadingLabel.contentSize.width)/2 -10,
                                      (self.frame.size.height - loadingLabel.contentSize.height)/2+containrect.size.height -20,
                                      loadingLabel.contentSize.width+20,
                                      loadingLabel.contentSize.height+10)];
    [container setFrame:containrect];
    [spinner setFrame:spinrect];
    [containerOfAll setFrame:CGRectMake(loadingLabel.frame.origin.x
                                        , containrect.origin.y, containrect.size.width+loadingLabel.contentSize.width +30, containrect.size.height+loadingLabel.contentSize.height +50)];
    
      containerOfAll.center = self.center;
//    [loadingLabel setFrame:textrect];
}



#pragma mark -
#pragma mark Class methods

+ (LoadingView *)showLoadingViewAddedTo:(UIView *)view {
    LoadingView *loadingView = [[LoadingView alloc] initWithFrame:view.bounds];
    [loadingView.loadingLabel setText:@"Loading"];

	[view addSubview:loadingView];
	return loadingView ;
}

+ (void)hideLoadingViewForView:(UIView *)view {
    

	for (UIView *v in [view subviews]) {
		if ([v isKindOfClass:[LoadingView class]]) {
            [v removeFromSuperview];
            break;
		}
	}
}



+(void)setText:(NSString *)txt ForView :(UIView *)view{
    dispatch_async(dispatch_get_main_queue(), ^{

    LoadingView *loadView;
    for (UIView *v in [view subviews]) {
		if ([v isKindOfClass:[LoadingView class]]) {
            loadView = (LoadingView *)v;
            break;
		}
	}
    
    [loadView.loadingLabel setText:txt];
   [loadView.loadingLabel setFrame:CGRectMake(0.0f, 0.0f, 600.0f, 20.0f)];

    [loadView setNeedsLayout];
//    [loadingView.loadingLabel setFrame:CGRectMake((loadingView.frame.size.width - loadingView.loadingLabel.contentSize.width)/2,
//                                      (loadingView.frame.size.height - loadingView.loadingLabel.contentSize.height)/2+30.0f,
//                                      loadingView.loadingLabel.contentSize.width,
//                                      loadingView.loadingLabel.contentSize.height)];
    });
}
#pragma mark -

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
