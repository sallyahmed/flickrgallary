//
//  GridViewController.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
@interface GridViewController : UIViewController<UISearchBarDelegate,ServerManagerDelegate,UICollectionViewDataSource ,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *grid;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
