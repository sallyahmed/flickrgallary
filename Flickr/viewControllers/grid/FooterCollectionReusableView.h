//
//  FooterCollectionReusableView.h
//  Flickr
//
//  Created by Sally Ahmed on 2/7/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterCollectionReusableView : UICollectionReusableView

@end
