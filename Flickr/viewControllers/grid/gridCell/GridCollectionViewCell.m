//
//  GridCollectionViewCell.m
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "GridCollectionViewCell.h"

@implementation GridCollectionViewCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"GridCollectionViewCell" owner:self options:nil];
        
        for (id view in arrayOfViews) {
            if ([view isKindOfClass:[GridCollectionViewCell class]]) {
                self = view;
                break;
            }
        }
        
    }
    return self;
    
}



- (void)layoutSubviews
{
    [super layoutSubviews];
    
    BOOL contentViewIsAutoresized = CGSizeEqualToSize(self.frame.size, self.contentView.frame.size);
    
    if( !contentViewIsAutoresized) {
        CGRect contentViewFrame = self.contentView.frame;
        contentViewFrame.size = self.frame.size;
        self.contentView.frame = contentViewFrame;
    }
}
@end
