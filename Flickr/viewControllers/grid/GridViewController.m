//
//  GridViewController.m
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "GridViewController.h"
#import "ServerManager.h"
#import "LoadingView.h"
#import "ApplicationController.h"
#import "FlickrModel.h"
#import "GridCollectionViewCell.h"
#import "FlickrObject.h"
#import "FooterCollectionReusableView.h"
#import "NSURLConnection+ActuallyAsynchronousConnection.h"
@interface GridViewController ()

@end

@implementation GridViewController
NSMutableArray * photosData;
UICollectionViewFlowLayout *layout;
int currentPageIndex;
NSMutableArray * connections;
- (void)viewDidLoad {
    [super viewDidLoad];
    ///[self.searchBar becomeFirstResponder];
    layout = [[UICollectionViewFlowLayout alloc]init];
    connections = [NSMutableArray array];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.searchBar setDelegate:self];
    [self.grid reloadData];

}

-(void)viewWillDisappear:(BOOL)animated{
    [self.searchBar setDelegate:nil];
    [super viewWillDisappear:animated];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.grid reloadData];
    
}
/*
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UisearchbarDelegation  - 

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [LoadingView showLoadingViewAddedTo:self.view];
    currentPageIndex = 0 ;
    [self getPhotos:searchBar.text atPageIndex:currentPageIndex];
}

-(void)getPhotos:(NSString *)str atPageIndex:(int)pageIndex{
    ServerManager * connection = [[ServerManager alloc]initWithDelegate:self];
    [connection searchOFPhotosWithText:str andPageIndex:currentPageIndex];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    // You can write search code Here
  

}
#pragma mark - serverMangerDelegation -

-(void)serverConnection:(id)connection ISFinishSuccessWithData:(id) data{
    
    if ([[[data objectForKey:@"photos"]objectForKey:@"total"] isEqualToString:@"0" ] ) {
        [self performSelectorOnMainThread:@selector(hideLoaderAndAdjustCollection) withObject:nil waitUntilDone:YES];

        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"No Result" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES ];
    }
    else{
        if (currentPageIndex ==  0 ) {
            photosData = [NSMutableArray array];
            //cancel all previous connections
            
            for (id connection in connections ) {
               
                [NSURLConnection cancelActuallyAsynchronousRequest:connection];
                
            }
            connections = [NSMutableArray array];
            [[FlickrModel getInstance]setFlickrObject:data];
            [[FlickrModel getInstance]setSmallPhotosUrls];
        }
        else {
            [[FlickrModel getInstance]appendFlickrObject:data];

        }
    
        
        [self performSelectorOnMainThread:@selector(hideLoadingViewAndAdjustPhotosForView) withObject:nil waitUntilDone:YES];

    }

}
-(void)serverConnection:(id)connection ISFailedByReason:(NSError *)error{
    [self performSelectorOnMainThread:@selector(hideLoaderAndAdjustCollection) withObject:nil waitUntilDone:YES];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES ];
    
}
#pragma  mark - actions -
-(void)hideLoaderAndAdjustCollection{
    [LoadingView hideLoadingViewForView:self.view];

    photosData = [NSMutableArray array];

    [self adjustCollectionView];

}
-(void)hideLoadingViewAndAdjustPhotosForView{
    [LoadingView hideLoadingViewForView:self.view];
    
  
    for (int i = 0 ; i <[[[FlickrModel getInstance]getFlickrObjects ] count]  ; i++) {
        
            [photosData addObject:@""];

        
    }
       [self adjustCollectionView];
 
}
-(void)adjustCollectionView{
    [self.grid registerClass:[GridCollectionViewCell class] forCellWithReuseIdentifier:@"GridCollectionViewCell"];
    [self.grid registerClass:[FooterCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterCollectionReusableView"];
    
    // setting cell attributes globally via layout properties ///////////////
    layout.itemSize = CGSizeMake(self.view.frame.size.width/3,self.view.frame.size.height/3);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.sectionInset = UIEdgeInsetsMake(0 , 0, 0, 0);
    [self.grid setCollectionViewLayout:layout];
    [self.grid setDataSource:self];
    [self.grid setDelegate:self];
    [self.grid reloadData];
}


#pragma mark - UICollectionDelegation -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [photosData count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"GridCollectionViewCell";
    
    GridCollectionViewCell*cell = (GridCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    
    NSURL *url = [NSURL URLWithString:[[[FlickrModel getInstance]getSmallPhotos]objectAtIndex:indexPath.row]];
   
    if (![[photosData objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
        cell.img.image = [[UIImage alloc] initWithData:[photosData objectAtIndex:indexPath.row]];
        cell.img.alpha = 0 ;
        [UIView animateWithDuration:1 delay:0.0 options:UIViewAnimationOptionCurveEaseOut
                         animations:^{ cell.img.alpha = 1.0;}
                         completion:nil];
        }
    else{
        UIImageView *recipeImageView = (UIImageView *)cell.img;
        recipeImageView.image = [UIImage imageNamed:@"noimage"];

    [self downloadImageWithURL:url andIndex :(int)indexPath.row completionBlock:^(BOOL succeeded, NSData *data) {
        if (succeeded) {
            if (indexPath.row == cell.tag)

            [UIView transitionWithView:recipeImageView
                              duration:0.4
                               options:UIViewAnimationOptionTransitionFlipFromRight
                            animations:^{
                                //  Set the new image
                                    cell.img.image = [[UIImage alloc] initWithData:data];
                               
                                
                            } completion:^(BOOL finished) {
                                //  Do whatever when the animation is finished
                            }];
        }
    }];
    }
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    // ainmattion with swipe view
    
    [[ApplicationController getInstance]gotoPagerWithPhotos:photosData withIndex:(int)indexPath.row];
    
}

//-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
////    UICollectionReusableView *reusableview = nil;
////
////    
////    if (kind == UICollectionElementKindSectionFooter) {
////        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterCollectionReusableView" forIndexPath:indexPath];
////        
////        reusableview = footerview;
////        [reusableview setBackgroundColor:[UIColor whiteColor]];
////    }
////    
////    return reusableview;
//}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return CGSizeMake(self.view.frame.size.width/3,self.view.frame.size.height/3);
}
- (void)downloadImageWithURL:(NSURL *)url andIndex :(int)index completionBlock:(void (^)(BOOL succeeded, NSData *data))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    // used it for cancel prvious connecton 
    id item = [NSURLConnection sendActuallyAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (!connectionError) {
            [photosData replaceObjectAtIndex:index withObject:data];
            
            completionBlock(YES, data);
        } else {
            completionBlock(NO, nil);
        }
    }];
    [connections addObject:item];
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        if (!error) {
//            [photosData replaceObjectAtIndex:index withObject:data];
//            
//            completionBlock(YES, data);
//        } else {
//            completionBlock(NO, nil);
//        }
//    }];
}
@end
