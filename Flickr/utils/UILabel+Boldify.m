//
//  UILabel+Boldify.m
//  EduWaveProject
//
//  Created by Sally Ahmed on 1/23/14.
//  Copyright (c) 2014 Sally. All rights reserved.
//

#import "UILabel+Boldify.h"

@implementation UILabel (Boldify)
- (void)boldRange:(NSRange)range {
    if (![self respondsToSelector:@selector(setAttributedText:)]) {
        return;
    }
    NSMutableAttributedString *attributedText;
    if (!self.attributedText) {
        attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    } else {
        attributedText = [[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    }
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Bold" size:12]} range:range];
    
    
    self.attributedText = attributedText;
}


- (void)ColorRange:(NSRange)range byColor :(UIColor * )color{
    if (![self respondsToSelector:@selector(setAttributedText:)]) {
        return;
    }
    NSMutableAttributedString *attributedText;
    if (!self.attributedText) {
        attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    } else {
        attributedText = [[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    }
    [attributedText setAttributes:@{NSForegroundColorAttributeName:color} range:range];
    
    
    self.attributedText = attributedText;
}
- (void)boldSubstring:(NSString*)substring {
    NSRange range = [self.text rangeOfString:substring];
    [self boldRange:range];
}

- (void)colorSubstring:(NSString*)substring  ofColor :(UIColor *)color{
    NSRange range = [self.text rangeOfString:substring];
    [self ColorRange:range byColor:color];
}
- (CGSize)contentSize
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = self.lineBreakMode;
    paragraphStyle.alignment = self.textAlignment;
    CGRect contentFrame ;

        NSAttributedString* string = [[NSAttributedString alloc]initWithString:self.text attributes:@{ NSFontAttributeName : self.font }];
     contentFrame =   [string boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.bounds), MAXFLOAT)
                                     options:NSStringDrawingTruncatesLastVisibleLine
                                           context:nil];
    
    
        return contentFrame.size;
}

- (void)sizeToFitWithAlignmentRight {
    CGRect beforeFrame = self.frame;
    [self sizeToFit];
    CGRect afterFrame = self.frame;
    self.frame = CGRectMake(beforeFrame.origin.x + beforeFrame.size.width - afterFrame.size.width, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}


@end
