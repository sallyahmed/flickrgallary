//
//  UILabel+Boldify.h
//  EduWaveProject
//
//  Created by Sally Ahmed on 1/23/14.
//  Copyright (c) 2014 Sally. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Boldify)
- (void) boldSubstring: (NSString*) substring;
- (void) boldRange: (NSRange) range;
- (CGSize)contentSize;
- (void)sizeToFitWithAlignmentRight ;
- (void)colorSubstring:(NSString*)substring  ofColor :(UIColor *)color;
@end
