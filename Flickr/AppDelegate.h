//
//  AppDelegate.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    ApplicationController *applicationController;

}
@property (strong, nonatomic) UIWindow *window;


@end

