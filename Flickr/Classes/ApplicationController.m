//
//  ApplicationController.m
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "ApplicationController.h"
#import "GridViewController.h"
#import "SwipeViewController.h"
@implementation ApplicationController
static ApplicationController *singletone = nil;
@synthesize window,application,queue;

+ (ApplicationController*)getInstance {
    if(singletone == nil) {
        
        singletone = [ApplicationController alloc];
        singletone.queue = [[NSOperationQueue alloc]init];
        [singletone.queue setMaxConcurrentOperationCount:4];
    }
    return singletone;
}


- (void)initWindow:(UIWindow*)w{
    window = w;
    self.baseNavigationController= [[UINavigationController alloc]init];
    
    [self.baseNavigationController.navigationBar setHidden:YES];
    GridViewController * gridVC = [[GridViewController alloc]initWithNibName:@"GridViewController" bundle:nil];
    
    [self.baseNavigationController setViewControllers:@[gridVC]];
    
    [self.window setRootViewController:self.baseNavigationController];
    self.baseNavigationController.navigationBar.opaque = YES;
    self.baseNavigationController.navigationBar.translucent = NO;

    
    
    
}

-(void)gotoPagerWithPhotos :(NSMutableArray * )photos  withIndex:(int) index{
    
    SwipeViewController * swipeVC = [[SwipeViewController alloc]initWithNibName:@"SwipeViewController" bundle:nil];
    
    swipeVC.photoData = photos;
    swipeVC.beginIndex = index;
    [self.baseNavigationController.navigationBar setHidden:NO];
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.baseNavigationController pushViewController:swipeVC animated:NO];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.baseNavigationController.view cache:NO];
    [UIView commitAnimations];
    
    
}
@end
