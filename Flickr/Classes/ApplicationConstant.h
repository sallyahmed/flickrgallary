//
//  ApplicationConstant.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationConstant : NSObject
#define FlickrAPIKey @"2f852b9c9b79a4ab5af440f3e56be1cb"


#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@end
