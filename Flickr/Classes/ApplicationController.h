//
//  ApplicationController.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ApplicationController : NSObject

@property (nonatomic , strong) UIApplication *application;
@property (nonatomic , strong) UIWindow *window;
@property (nonatomic , strong) NSOperationQueue *queue;
@property (nonatomic , strong) UINavigationController * baseNavigationController;

+ (ApplicationController*)getInstance;
- (void)initWindow:(UIWindow*)w;
-(void)gotoPagerWithPhotos :(NSMutableArray * )photos withIndex:(int) index;
@end
