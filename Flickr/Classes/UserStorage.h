//
//  UserStorage.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserStorage : NSObject
+(void)setFlickrObjects :(NSMutableArray *)objs;
+(NSMutableArray *)getFlickrObjectst;
+(void)setsetSmallPhotosUrls:(NSMutableArray *)photoURls;
+(NSMutableArray *)getsetSmallPhotosUrls;


@end
