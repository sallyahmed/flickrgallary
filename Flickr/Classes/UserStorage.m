//
//  UserStorage.m
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "UserStorage.h"

@implementation UserStorage
static NSMutableArray *flickrObjects;
static NSMutableArray *photoUrls;

+(void)setFlickrObjects :(NSMutableArray *)objs{
    flickrObjects= objs;
}
+(NSMutableArray *)getFlickrObjectst{
    return flickrObjects;
}
+(void)setsetSmallPhotosUrls:(NSMutableArray *)URls{
    photoUrls = URls;
}
+(NSMutableArray *)getsetSmallPhotosUrls{
    return photoUrls;
}
@end
