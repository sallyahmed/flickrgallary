//
//  ServerManager.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ServerManagerDelegate <NSObject>
-(void)serverConnection:(id)connection ISFinishSuccessWithData:(id) data;
-(void)serverConnection:(id)connection ISFailedByReason:(NSError *)error;

@end

@interface ServerManager : NSObject<NSURLConnectionDelegate,NSURLConnectionDataDelegate>
@property (nonatomic,weak)id<ServerManagerDelegate> delegate;

-(id)initWithDelegate:(id <ServerManagerDelegate>)delegate;


+(int) checkInternetConnection ;

+(void)startNotify;


-(void)searchOFPhotosWithText :(NSString *)searchStr andPageIndex:(int)pageIndex;
@end
