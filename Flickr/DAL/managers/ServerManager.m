//
//  ServerManager.m
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "ServerManager.h"
#import "Reachability.h"
#import "ApplicationConstant.h"
#import "SBJson.h"
@implementation ServerManager
Reachability *reach;
NSURLConnection *connection;
NSMutableData*receivedData;

NSURL *url;
NSURLRequest *request;

-(id)initWithDelegate:(id <ServerManagerDelegate>)delegate{
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

+(int) checkInternetConnection {
    
    reach = [Reachability reachabilityForInternetConnection];
    // reach = [Reachability reachabilityWithHostname:WEB_SERVICE_URL];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        
        //connection good  >> check for update
        
        
    };
    
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (netStatus)
            {
                    
                case ReachableViaWWAN:
                {
                    
                    break;
                }
                case ReachableViaWiFi:
                {
                    
                    break;
                }
                case NotReachable:
                {
                    
                    
                    break;
                }
            }
            
            
        });
    };
    
    
    return netStatus;
    
}

+(void)startNotify{
    [reach startNotifier];
    
}


-(void)searchOFPhotosWithText :(NSString *)searchStr andPageIndex:(int)pageIndex{
    receivedData =[[NSMutableData alloc]init];
    // Build the stri[]ng to call the Flickr API
    NSString *urlString = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&tags=%@&per_page=30&format=json&nojsoncallback=1&page=%d", FlickrAPIKey, searchStr,pageIndex];
    
    // Create NSURL string from formatted string
    url = [NSURL URLWithString:urlString];
    
    // Setup and start async download
    request = [[NSURLRequest alloc] initWithURL: url];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}


#pragma mark - NSURLConnectionDelegate -

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [receivedData appendData:data];


}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
  //  [self jsonDataLoad:receivedData];
    // Store incoming data into a string
    NSString *jsonString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    // Create a dictionary from the JSON string
    NSDictionary *results = [jsonString JSONValue];
    NSLog(@"%@" , results);
    [self.delegate serverConnection:self ISFinishSuccessWithData:results];

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.delegate serverConnection:self ISFailedByReason:error];
}

@end
