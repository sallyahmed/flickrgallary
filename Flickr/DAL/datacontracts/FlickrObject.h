//
//  FlickrObject.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrObject : NSObject
@property(nonatomic,strong) NSString* ID;
@property(nonatomic,strong) NSString* secret;
@property(nonatomic,strong) NSString* farm;
@property(nonatomic,strong) NSString* server;
@property(nonatomic ,strong)NSString* title;
-(id)initWithDictionary:(NSMutableDictionary *)dic;


@end
