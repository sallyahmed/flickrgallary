//
//  FlickrObject.m
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "FlickrObject.h"

@implementation FlickrObject

-(id)initWithDictionary:(NSMutableDictionary *)dic{
    self = [super init];
    if (self) {
    
        self.ID = [dic objectForKey:@"id"];
        self.secret = [dic objectForKey:@"secret"];
        self.farm = [dic objectForKey:@"farm"];
        self.server = [dic objectForKey:@"server"];
        self.title = [dic objectForKey:@"title"];
    }
    return self;
    
}
@end
