//
//  FlickrModel.h
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrModel : NSObject
+ (FlickrModel*)getInstance;
-(void)setFlickrObject:(NSMutableDictionary *)objs;
-(void)setSmallPhotosUrls;
-(void)appendFlickrObject:(NSMutableDictionary *)results;
-(void)setSLargePhotosUrls:(NSMutableArray * ) photos;
-(NSMutableArray * )getFlickrObjects;
-(NSMutableArray *)getSmallPhotos;
@end
