//
//  FlickrModel.m
//  Flickr
//
//  Created by Sally Ahmed on 2/3/15.
//  Copyright (c) 2015 Sally Ahmed. All rights reserved.
//

#import "FlickrModel.h"
#import "FlickrObject.h"
#import "UserStorage.h"
@implementation FlickrModel
static FlickrModel *singletone = nil;

+ (FlickrModel*)getInstance {
    if(singletone == nil) {
        
        singletone = [FlickrModel alloc];
    }
    return singletone;
}

-(void)setFlickrObject:(NSMutableDictionary *)results{
    NSArray *objs = [[results objectForKey:@"photos"] objectForKey:@"photo"];

    NSMutableArray* result =[NSMutableArray array];
    for (NSMutableDictionary * dic in objs) {
        FlickrObject * obj = [[FlickrObject alloc]initWithDictionary:dic];
        [result addObject:obj];
    }
    [UserStorage setFlickrObjects:result];
    
}

-(void)appendFlickrObject:(NSMutableDictionary *)results{
    
    NSArray *objs = [[results objectForKey:@"photos"] objectForKey:@"photo"];
    
    NSMutableArray* result =[NSMutableArray array];
    for (NSMutableDictionary * dic in objs) {
        FlickrObject * obj = [[FlickrObject alloc]initWithDictionary:dic];
        [result addObject:obj];
    }

    
    NSMutableArray * oldResults=  [UserStorage getFlickrObjectst];
    [oldResults addObjectsFromArray:result];
    
    [UserStorage setFlickrObjects:oldResults];
    
}
-(NSMutableArray * )getFlickrObjects{
    return [UserStorage getFlickrObjectst];
}
-(void)setSmallPhotosUrls{
    NSMutableArray* result =[NSMutableArray array];

    for (FlickrObject * obj  in self.getFlickrObjects) {
        
        NSString *photoURLString =
        [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_m.jpg",
         obj.farm,obj.server,
         obj.ID, obj.secret];
        //[result addObject:[NSData dataWithContentsOfURL:[NSURL URLWithString:photoURLString]]];
        [result addObject:photoURLString];
    }
    [UserStorage setsetSmallPhotosUrls:result];
    
    
}
-(NSMutableArray *)getSmallPhotos{
   return  [UserStorage getsetSmallPhotosUrls];
}
-(void)setSLargePhotosUrls:(NSMutableArray * ) photos{
    
}
@end
